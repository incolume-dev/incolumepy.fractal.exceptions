__version__ = "0.1.0"


class NiHilError(Exception):
    """NiHilError"""


class InanisError(Exception):
    """InanisError"""


class SuppliciumError(Exception):
    """SuppliciumError"""


class OmniError(Exception):
    """Omni Error"""
