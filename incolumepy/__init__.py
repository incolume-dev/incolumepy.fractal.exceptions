from pathlib import Path

try:
    __path__ += [
        Path(__file__).resolve().parent.joinpath("fractal").as_posix(),
        Path(__file__).resolve().parent.joinpath("fractal", "exceptions").as_posix(),
    ]
except NameError:

    __path__ = [
        Path(__file__).resolve().parent.joinpath("fractal").as_posix(),
        Path(__file__).resolve().parent.joinpath("fractal", "exceptions").as_posix(),
    ]


if __name__ == "__main__":
    print(__path__)
